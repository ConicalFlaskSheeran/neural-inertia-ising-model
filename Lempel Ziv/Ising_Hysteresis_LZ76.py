#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 20 15:38:07 2021

@author: corneliasheeran
"""
import numpy as np
import math
import matplotlib.pyplot as plt
import random
import copy
from tqdm import tqdm
import scipy.io
from lz76 import LZ76 as LZ

####################
"EDIT HERE:"
"Choose Parameters"
field_size = 25             #no. values of the field 
cycles     = 6              #number of sweeps over configuration
SC         = np.loadtxt('DTI_fiber_consensus_HCP.csv', delimiter=',')  #Schaefer parcellation matrix
mat        = scipy.io.loadmat('/Users/corneliasheeran/wetransfer-bc2459/GABAA_PET_density_Schaefer100.mat') #GABBA map
speed      = 8 #infusion speed

####################

"Inputs"
dens_map      = mat['density_map'] #GABBA density vector
size          = SC.shape[0]        #no. Schaefer parcellations (connectome)
norm_scale    = np.tile((dens_map/dens_map.max()).reshape(-1), 1) #GABBA scaling factor for field 

####################

"Interaction Matrices: Random, Original, Mixed"

#Random J
np.random.seed(42)
b      = np.random.uniform(low = 0, high = 1, size=(size, size))            
c      = (b + b.T - np.diag(b.diagonal()))     
Rand_J      = c/c.max()

for i in range(0, size):
    Rand_J[i, i] = 0
    
Rand_scale  = np.random.uniform(low = 0, high = 1, size=(size,))   
       

#Original J
Original_J        = (SC/SC.max())


#Mixed J
Mixed_J           = (SC/SC.max())
m                 = ~np.eye(len(Mixed_J ), dtype=bool) # mask of non-diagonal elements
idx               = np.flatnonzero(m)
Mixed_J.flat[idx] = Mixed_J .flat[np.random.permutation(idx)]
                                                               
"Fields: Linear, Pharmacokinetic"
#Linear Field"
linear_field      = np.linspace(0, 0.25, num=field_size)


#Pharmacokinetic Field
inputfield0       = np.zeros((1,))
inputfield1       = np.linspace(0, 1.5, num=field_size)
inputfield2       = np.linspace(1.5, 5, num=field_size)

def first_dose_grad(x):
    y = speed/(np.cosh(speed*x)**2)
    return y

def metabolise_grad(x):
    y = -2*np.exp(-(2*x-3)) 
    return y

def first_dose(x):
    y = np.tanh(speed*x)
    return y

def metabolise(x):
    y = np.exp(-(2*x - 3))
    return y

PK_field  = np.concatenate((inputfield0, first_dose(inputfield1), metabolise(inputfield2), inputfield0), axis = 0)
PK_field1 = np.concatenate((inputfield0, first_dose(inputfield1)), axis = 0)
PK_field2 = np.concatenate((metabolise(inputfield2), inputfield0), axis = 0)


"Starting Random Configuration"
config = (2*np.random.randint(2, size=size*1)-1) 

####################
"Required Functions"

def calcMag(config):
    '''Magnetization of a given configuration'''
    
    Mag = np.sum(config)/size
    
    return Mag


def calcEnergy(J, config, field, scale):
    '''Energy of a given configuration with external field'''
    
    E = -( 0.5*np.dot(np.dot(J, config), config) + field*np.sum(config*scale) )
    
    return E


def calcEnergy2(J, config):
    '''Energy of a given configuration without external field'''
    
    E2 = -(0.5*np.dot(np.dot(J, config), config))
    
    return E2


####################
"EDIT HERE:"
"Choose Field, Connection Matrix and Temperature"

field_name = 'PK field'
J_name     = 'Original'
Temp       = np.array([3])
beta       = 1/Temp

####################
C_mean = []

if field_name == 'Field (Linear)':
    
    field      = linear_field
    flip_field = np.flip(field)
    hyst_field = np.tile(np.concatenate((field, flip_field), axis=0), 1)
    points     = len(hyst_field)

else:
    hyst_field = PK_field
    points     = len(hyst_field)

if J_name == 'Original':
    J = Original_J
    scale = norm_scale
elif J_name == 'Random':
    J = Rand_J
    scale = Rand_scale
else:
    J = Mixed_J
    scale = norm_scale

#Metropolis Algorithm:
    
for j, b in enumerate(beta):
     
    configh = copy.deepcopy(config)
    data    = np.zeros((size, cycles, points)) #create data array
    
    for p in tqdm(range(0, points)):
    
        f = hyst_field[p]
        data = np.zeros((size, cycles, size)) #create data array
        
        for c in range (0, cycles):
            
            index = random.sample(range(0, size), size) #randomly pick a spin to flip  
            
            for i in index:
    
                confignew = copy.deepcopy(configh) #new configuration to test flipped spin
                confignew[i] = confignew[i] * -1
                
                NewE = calcEnergy(J, confignew, f, scale)
                OldE = calcEnergy(J, configh, f, scale)
                
                delta_E = NewE - OldE
                
                x = random.uniform(0, 1)
                prob = math.exp(-delta_E * beta)
                
                if delta_E <= 0:            
                    configh = confignew
                elif prob > x:
                    configh = confignew
                
                data[:, c, i] = configh
                
        dataeq = data[:, int(cycles/2):cycles, :]
        final = np.reshape( dataeq, (size, int((cycles/2)*size)) )
    
        C_val = np.zeros((size))
        
        for i in range(0, size):
            binary = np.where(final[i, :] == -1, 0, final[i, :])
            C_val[i] = LZ(binary)
            
        C_mean.append(np.mean(C_val))
    

    if field_name == 'Field (Linear)':
        
        plt.figure(j)
        plt.plot(hyst_field[0:int(points/2)], C_mean[0:int(points/2)], label = 'Forwards')
        plt.plot(hyst_field[int(points/2):points], C_mean[int(points/2):points], label = 'Backwards')    
        plt.legend(loc="lower right")
        plt.xlabel(f"{field_name}")
        plt.ylabel("Complexity")
        plt.title(f"C vs {field_name}, Temp={1/b}, J={J_name}") 
        plt.grid()
    
        
    else:
        plt.figure(j)
        plt.plot(PK_field1, C_mean[0 : PK_field1.shape[0]], label = 'Forwards')
        plt.plot(PK_field2, C_mean[PK_field1.shape[0] : PK_field.shape[0]], label = 'Backwards')       
        plt.legend(loc="lower right")
        plt.xlabel(f"{field_name}")
        plt.ylabel("Complexity")
        plt.title(f"C vs {field_name}, Temp={1/b}, Infusion Speed={speed}, J={J_name}") 
        plt.grid()
