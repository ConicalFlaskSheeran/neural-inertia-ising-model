#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 20 13:17:13 2021

@author: corneliasheeran
"""
import numpy as np
import math
import matplotlib.pyplot as plt
import random
import copy
from tqdm import tqdm
import scipy.io
from scipy import stats
import pandas as pd
import seaborn as sns
import warnings

warnings.filterwarnings("ignore")
warnings.warn("PearsonRConstantInputWarning")
warnings.warn("RuntimeWarning")

####################
#Indicies for each RSN in the Schaefer parcellation matrix
vis           = [0, 9, 50, 58, 'Vis']
Som_Mot       = [9, 15, 58, 66, 'Som_Mot']
Dors_Attn     = [15, 23, 66, 73, 'Dors_Attn']
Sal_Vent_Attn = [23, 30, 73, 78, 'Sal_Vent_Attn']
Limbic        = [30, 33, 78, 80, 'Limbic']
Cont          = [33, 37, 80, 89, 'Cont']
Default       = [37, 50, 89, 100, 'Default']

####################
"EDIT HERE:"
field_size  = 25             #no. values of the field 
max_field   = 0.25
cycles      = 2000           #number of sweeps over configuration
SC          = np.loadtxt('DTI_fiber_consensus_HCP.csv', delimiter=',')  #Schaefer parcellation matrix
mat         = scipy.io.loadmat('/Users/corneliasheeran/wetransfer-bc2459/GABAA_PET_density_Schaefer100.mat') #GABBA map
speed       = 8 #infusion speed
field_name  = 'Field (Linear)'
J_name      = "Original"
Temp        = 3
Choose_Mean = "Neg Mean"
one_RSN     = vis #'Whole Brain'
two_RSN     = Som_Mot

####################
"Inputs"
dens_map = mat['density_map'] #GABBA density vector
size     = SC.shape[0]        #no. Schaefer parcellations (connectome)
beta  = 1/Temp
####################

if J_name == 'Original':
    
    J     = (SC/SC.max())
    scale = np.tile((dens_map/dens_map.max()).reshape(-1), 1) #GABBA scaling factor for field

    
elif J_name == 'Random':
    
    np.random.seed(42)
    b      = np.random.uniform(low = 0, high = 1, size=(size, size))            
    c      = (b + b.T - np.diag(b.diagonal()))     
    J      = c/c.max()
    
    for i in range(0, size):
        J[i, i] = 0
    
    scale  = np.random.uniform(low = 0, high = 1, size=(size,)) 

    
else:
    
    J           = (SC/SC.max())
    m           = ~np.eye(len(J ), dtype=bool) # mask of non-diagonal elements
    idx         = np.flatnonzero(m)
    J.flat[idx] = J .flat[np.random.permutation(idx)]

    scale       = np.tile((dens_map/dens_map.max()).reshape(-1), 1)


if field_name == 'Field (Linear)':
    
    field      = np.linspace(0, max_field, num=field_size)
    flip_field = np.flip(field)
    hyst_field = np.tile(np.concatenate((field, flip_field), axis=0), 1)
    points     = len(hyst_field)

else:
    inputfield0 = np.zeros((1,))
    inputfield1 = np.linspace(0, 1.5, num=field_size)
    inputfield2 = np.linspace(1.5, 5, num=field_size)
    
    def first_dose(x):
        y = np.tanh(speed*x)
        return y
    
    def metabolise(x):
        y = np.exp(-(2*x - 3))
        return y
    
    PK_field  = np.concatenate((inputfield0, first_dose(inputfield1), metabolise(inputfield2), inputfield0), axis = 0)
    PK_field1 = np.concatenate((inputfield0, first_dose(inputfield1)), axis = 0)
    PK_field2 = np.concatenate((metabolise(inputfield2), inputfield0), axis = 0)

    hyst_field = PK_field
    points     = len(hyst_field)  

####################
"Starting Random Configuration"
config = (2*np.random.randint(2, size=size*1)-1) 

####################
"Required Functions"

def calcMag(config):
    '''Magnetization of a given configuration'''
    
    Mag = np.sum(config)/size
    
    return Mag


def calcEnergy(J, config, field, scale):
    '''Energy of a given configuration with external field'''
    
    E = -( 0.5*np.dot(np.dot(J, config), config) + field*np.sum(config*scale) )
    
    return E
    
####################  

corr_mean = [] #List of correlation means
configh = copy.deepcopy(config)

if one_RSN == 'Whole Brain':
    for j, f in tqdm(enumerate(field)):
     
        data = np.zeros((size, cycles, size)) #create data array
            
        for c in range (0, cycles):
            
            index = random.sample(range(0, size), size) #randomly pick a spin to flip  
            
            for i in index:
        
                confignew = copy.deepcopy(configh) #new configuration to test flipped spin
                confignew[i] = confignew[i] * -1
                
                NewE = calcEnergy(J, confignew, f, scale)
                OldE = calcEnergy(J, configh, f, scale)
                
                delta_E = NewE - OldE
                
                x = random.uniform(0, 1)
                prob = math.exp(-delta_E * beta)
                
                if delta_E <= 0:            
                    configh = confignew
                elif prob > x:
                    configh = confignew
                
                data[:, c, i] = configh
        
        dataeq = data[:, int(cycles/2):cycles, :]
        final = np.reshape( dataeq, (size, int((cycles/2)*size)) ) #Only use data from half of the cycles (to allow equilibration)
        
        corr = np.zeros((size,size))
        pval = np.zeros((size,size))
        
        for i in range(0, size): #create a matrix filled with correlation values between each brain region
            for k in range(0, size):
                corr_ik = scipy.stats.pearsonr(final[i, :], final[j, :])
    
                if corr_ik[1] <= 0.05: #check p-value <= 0.05
                    pval[i, k] = corr_ik[1]
                    corr[i, k] = corr_ik[0]
                
        corr = np.reshape(corr[~np.isnan(corr)], (size, size))
    
        
        "Pandas DataFrames"
        # df_corr = pd.DataFrame(data=corr, index=['vis_mag', 'som_mag', 'dors_mag', 'sal_mag', 'lim_mag', 'cont_mag', 'default_mag'], 
        #                   columns=['vis_mag', 'som_mag', 'dors_mag', 'sal_mag', 'lim_mag', 'cont_mag', 'default_mag'])
        
        # df_pval = pd.DataFrame(data=pval, index=['vis_mag', 'som_mag', 'dors_mag', 'sal_mag', 'lim_mag', 'cont_mag', 'default_mag'], 
        #                   columns=['vis_mag', 'som_mag', 'dors_mag', 'sal_mag', 'lim_mag', 'cont_mag', 'default_mag'])
        
        # df_corr.to_csv(f"corr_f={f}.csv", index=True)  
        # df_pval.to_csv(f"pval_f={f}.csv", index=True)  
        
        "Whole Corr heatmap" 
        # plt.figure(j)
        # ax1 = sns.heatmap(corr, vmin=-1, vmax=1, center= 0, cmap= 'coolwarm')
        # ax1.set_title(f'Whole corr, field = {f}')
        # plt.show()
        
    
        negative = corr[np.where(corr < 0)]
        neg_mean = np.nanmean(negative)
    
        tot_mean = np.nanmean(corr)
    
        
        if Choose_Mean == "Neg Mean":   
            if math.isnan(neg_mean) == True: #If 'negative' is empty, average corr is set = 0
                corr_mean.append(0)
            else:
                corr_mean.append(neg_mean)
        else:
            corr_mean.append(tot_mean)
            
    
    plt.figure(1000)
    plt.plot(field, corr_mean)
    plt.xlabel(f"{field_name}")
    plt.ylabel("Average Correlation")
    plt.title(f"Whole Brain, {Choose_Mean}, Temp={1/beta}") 
    plt.grid()
    plt.show()
        
else:
    
    for j, f in tqdm(enumerate(field)):
 
        data = np.zeros((size, cycles, size)) #create data array
            
        for c in range (0, cycles):
            
            index = random.sample(range(0, size), size) #randomly pick a spin to flip  
            
            for i in index:
        
                confignew = copy.deepcopy(configh) #new configuration to test flipped spin
                confignew[i] = confignew[i] * -1
                
                NewE = calcEnergy(J, confignew, f, scale)
                OldE = calcEnergy(J, configh, f, scale)
                
                delta_E = NewE - OldE
                
                x = random.uniform(0, 1)
                prob = math.exp(-delta_E * beta)
                
                if delta_E <= 0:            
                    configh = confignew
                elif prob > x:
                    configh = confignew
                
                data[:, c, i] = configh
        
        dataeq = data[:, int(cycles/2):cycles, :]
        final = np.reshape( dataeq, (size, int((cycles/2)*size)) )
        
        corr = np.zeros((size,size))
        pval = np.zeros((size,size))
        
        for i in range(0, size):
            for k in range(0, size):
                corr_ik = scipy.stats.pearsonr(final[i, :], final[j, :])
    
                if corr_ik[1] <= 0.05: #check p-value <= 0.05
                    pval[i, k] = corr_ik[1]
                    corr[i, k] = corr_ik[0]
                
        corr = np.reshape(corr[~np.isnan(corr)], (size, size))
        
        first       = np.concatenate(( corr[one_RSN[0]:one_RSN[1] + 1, :], corr[one_RSN[2]:one_RSN[3] + 1, :]),  axis=0)
        second      = np.concatenate(( first[:, two_RSN[0]:two_RSN[1] + 1], first[:, two_RSN[2]:two_RSN[3] + 1]), axis=1)
        
        "Pandas DataFrames"
        # df_corr = pd.DataFrame(data=corr, index=['vis_mag', 'som_mag', 'dors_mag', 'sal_mag', 'lim_mag', 'cont_mag', 'default_mag'], 
        #                   columns=['vis_mag', 'som_mag', 'dors_mag', 'sal_mag', 'lim_mag', 'cont_mag', 'default_mag'])
        
        # df_pval = pd.DataFrame(data=pval, index=['vis_mag', 'som_mag', 'dors_mag', 'sal_mag', 'lim_mag', 'cont_mag', 'default_mag'], 
        #                   columns=['vis_mag', 'som_mag', 'dors_mag', 'sal_mag', 'lim_mag', 'cont_mag', 'default_mag'])
        
        # df_corr.to_csv(f"corr_f={f}.csv", index=True)  
        # df_pval.to_csv(f"pval_f={f}.csv", index=True)  
        
        "Whole Corr heatmap"
        # plt.figure(j)
        # ax1 = sns.heatmap(corr, vmin=-1, vmax=1, center= 0, cmap= 'coolwarm')
        # ax1.set_title(f'Whole corr, field = {f}')
        # plt.show()
        
        
        "Resting State Networks Comparision Heatmap"
        # plt.figure(j+30)
        # ax2 = sns.heatmap(second, vmin=-0.3, vmax=0.3, center= 0, cmap= 'coolwarm')
        # ax2.set_title(f'DMN & Dors, field = {f}, ')
        # plt.show()
    
        negative = second[np.where(second < 0)]
        neg_mean = np.nanmean(negative)
    
        tot_mean = np.nanmean(second)
    
        
        if Choose_Mean == "Neg Mean":   
            if math.isnan(neg_mean) == True: #If 'negative' is empty, average corr is set = 0
                corr_mean.append(0)
            else:
                corr_mean.append(neg_mean)
        else:
            corr_mean.append(tot_mean)
            

    plt.figure(1000)
    plt.plot(field, corr_mean)
    plt.xlabel(f"{field_name}")
    plt.ylabel("Average Correlation")
    plt.title(f"{one_RSN[4]} & {two_RSN[4]}, {Choose_Mean}, Temp={1/beta}") 
    plt.grid()
    plt.show()
