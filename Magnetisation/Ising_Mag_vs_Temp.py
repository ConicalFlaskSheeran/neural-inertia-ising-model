#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 20 14:48:19 2021

@author: corneliasheeran
"""
import numpy as np
import math
import matplotlib.pyplot as plt
import random
import copy
from tqdm import tqdm

####################
"EDIT HERE"

cycles     = 6              #number of sweeps over configuration
SC         = np.loadtxt('DTI_fiber_consensus_HCP.csv', delimiter=',')  #Schaefer parcellation matrix
J_name     = 'Original'

max_Temp   = 0.1
min_Temp   = 4
Temp_size  = 100
Temp       = np.linspace(max_Temp, min_Temp, num=Temp_size)

####################

"Inputs"
size     = SC.shape[0]        #no. Schaefer parcellations (connectome)
beta     = 1/Temp
####################

"Starting Random Configuration"
config = (2*np.random.randint(2, size=size*1)-1) 

####################
"Required Functions"

def calcMag(config):
    '''Magnetization of a given configuration'''
    
    Mag = np.sum(config)/size
    
    return Mag


def calcEnergy2(J, config):
    '''Energy of a given configuration without external field'''
    
    E2 = -(0.5*np.dot(np.dot(J, config), config))
    
    return E2

#################### 

if J_name == 'Original':
    
    J     = (SC/SC.max())
    scale = np.tile((dens_map/dens_map.max()).reshape(-1), 1) #GABBA scaling factor for field
  
elif J_name == 'Random':
    
    np.random.seed(42)
    b      = np.random.uniform(low = 0, high = 1, size=(size, size))            
    c      = (b + b.T - np.diag(b.diagonal()))     
    J      = c/c.max()
    
    for i in range(0, size):
        J[i, i] = 0
    
    scale  = np.random.uniform(low = 0, high = 1, size=(size,)) 

else:
    
    J           = (SC/SC.max())
    m           = ~np.eye(len(J ), dtype=bool) # mask of non-diagonal elements
    idx         = np.flatnonzero(m)
    J.flat[idx] = J .flat[np.random.permutation(idx)]

    scale       = np.tile((dens_map/dens_map.max()).reshape(-1), 1)
  

point = len(beta)
data  = np.zeros((size, cycles, point)) #create data array

####################

for be in tqdm(range(0, point)):
    
    b = beta[be]
    configh = copy.deepcopy(config) #mag for each temp value is computed independently 

    for c in range (0, cycles):
        
        index = random.sample(range(0, size), size) #randomly pick a spin to flip 
        
        for i in index:

            confignew    = copy.deepcopy(configh) #new configuration to test flipped spin
            confignew[i] = confignew[i] * -1
            
            NewE = calcEnergy2(J, confignew)
            OldE = calcEnergy2(J, configh)
            
            delta_E = (NewE - OldE)
            
            x = random.uniform(0, 1)
            prob = math.exp(-delta_E * b)
            
            if delta_E <= 0:            
                configh = confignew
            elif prob > x:
                configh = confignew
            
            Mag = calcMag(configh)
            data[i, c, be] = Mag
    
length   = int(data.shape[1]/2)
              
mean_mag = np.abs(np.mean(data[:, length:length*2, :], axis=(1,0)))

plt.plot(Temp, mean_mag)
plt.xlabel("Temperature (arbitrary units)")
plt.ylabel("Magnitisation")
plt.title(f"Magnitisation vs Temperature, J = {J_name}") 
plt.grid()
plt.show()
