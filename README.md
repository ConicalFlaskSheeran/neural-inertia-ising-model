# Neural Inertia Ising Model

Modelling propofol induced neural inertia using a modified Ising model.

## Description
field = propofol dose

RSN = resting state network

**This repository contains:**

**In Magnetisation:**
1. _Ising_Hysteresis_
- Outputs plots of magnitisation vs field, at a constant temperature, as field is increased from 0 to 1 then reduced back to 0.

2. _Ising_Corr_
- Outputs a matrix (as an array or pandas dataframe) of the Pearson correlation coefficents between every pair of brain spins. 
- Outputs heatmaps of the correlation matrix (either whole brain or a between brain regions) at constant field and temperature values.
- Outputs plots of the mean correlation between two RSNs (or whole brain) vs field.

3. _Ising_Mag_vs_Temp_
- Outputs plots of magnitisation vs temperature, for no external field.

**In Lempel Ziv:**
1. _lz76_
- Lempel Ziv 76 algorthim 

2. _Ising_Hysteresis_LZ76_
- Outputs plots of complexity vs field, at a constant temperature, as field is increased from 0 to 1 then reduced back to 0.


## Parameters

| Parameter name | Default        | Description                                                                   |
|----------------|----------------|-------------------------------------------------------------------------------|
| field_size     | 25             | Number of field values between 0 and 1.                                       |
| max_field      | 0.25           | Maximum field value for field_name = 'Field (Linear)'.                        |
| cycles         | 2000           | Number of sweeps (number of spin-flip attempts for each brain spin).          |
| SC             | N/A            | Location of Schaefer parcellation matrix.                                     |
| mat            | N/A            | Location of GABBA density map.                                                |
| field_name     | Field (Linear) | Type of field (either: linear or pharmacokinetic).                            |
| J_name         | Original       | Type of interaction matrix (either: original, mixed or random).               |
| Temp           | 3              | Temperature.                                                                  |
| Speed          | 4              | Proxy measure for propofol infusion speed for field_name =  'pharmacokinetic' |

**- All parameters can be edited at the place(s) marked: EDIT HERE.**

_Ising_Corr_ also contains: 
1. Choose_Mean
- Mean of the correlation matrix or a slice of the correlation matrix outputted. 
- Either 'Total Mean' or 'Negative Mean' can be chosen. 
- Negative mean allows only the negative correlation values to be averaged.

2. one_RSN and two_RSN
- Pair of RSNs chosen.  The correlation matrix will then be sliced so only the correlation values of the chosen pair of RSNs are averaged over.
- A whole brain average can be chosen if one_RSN = 'Whole Brain'.

## References

1. Luppi, A. I., Spindler, L., Menon, D. K., & Stamatakis, E. A. (2021). The Inert Brain: Explaining Neural Inertia as Post-anaesthetic Sleep Inertia. Frontiers in neuroscience, 15, 643871. https://doi.org/10.3389/fnins.2021.643871

2. Schaefer, A., Kong, R., Gordon, E. M., Laumann, T. O., Zuo, X.-N., Holmes, A. J., Eickhoff, S. B., & Yeo, B. T. T.. (2018). Local-Global Parcellation of the Human Cerebral Cortex from Intrinsic Functional Connectivity MRI. Cerebral Cortex, 28(9), 3095–3114. https://doi.org/10.1093/cercor/bhx179

